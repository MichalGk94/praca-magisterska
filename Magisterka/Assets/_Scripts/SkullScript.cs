﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkullScript : MonoBehaviour {

    GameObject camp;
    GameObject player;
    Vector3 hoover;
    private float speed;
    Quaternion skull_rot;

	// Use this for initialization
	void Start () {

        camp = GameObject.Find("Campsite");
        player = GameObject.Find("Player");
        hoover = Vector3.up;
        speed =0.5f;

	}

    // Update is called once per frame
    void Update()
    {
        float height = Vector3.Distance(transform.position, camp.transform.position);

        Vector3 relativePos = player.transform.position - transform.position;
        skull_rot = Quaternion.LookRotation(relativePos);
        transform.LookAt(player.transform.position);

        if (hoover == Vector3.up) //jeśli leci do góry
        {
            if (height <= 2.5)
            {
                transform.Translate(hoover * speed * Time.deltaTime, Space.World);
            }
            else
            {
                hoover = Vector3.down;
                transform.Translate(hoover * speed * Time.deltaTime, Space.World);
            }
        }

        else if (hoover == Vector3.down) //jeśli opada
        {
            if (height >= 1.5)
            {
                transform.Translate(hoover * speed * Time.deltaTime, Space.World);
            }
            else
            {
                hoover = Vector3.up;
                transform.Translate(hoover * speed * Time.deltaTime, Space.World);
            }
        }

    }
    
}
