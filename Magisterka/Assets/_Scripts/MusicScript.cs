﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicScript : MonoBehaviour {

    Object[] soundtrack; // declare this as Object array
    private AudioSource ost;

    void Awake()
    {
        ost = GetComponent<AudioSource>();
        soundtrack = Resources.LoadAll("Music", typeof(AudioClip));
        ost.clip = soundtrack[0] as AudioClip;
    }

    void Start()
    {
        if (!ost.isPlaying)
        {
            ost.Play();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!ost.isPlaying)
            playRandomMusic();
    }

    void playRandomMusic()
    {
        ost.clip = soundtrack[Random.Range(0, soundtrack.Length-1)] as AudioClip;
        ost.Play();
    }

}
