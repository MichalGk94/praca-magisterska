﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hover : MonoBehaviour {

    public float hoverForce = 10f;

    void OnTriggerStay(Collider other)
    {
        other.GetComponentInParent<Rigidbody>().AddForce(Vector3.up * hoverForce, ForceMode.Acceleration);
    }

}
