﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PortalScript : MonoBehaviour {

    [SerializeField] public Slider insanitySlider;
    [SerializeField] ParticleSystem teleport;
    [SerializeField] ParticleSystem endGame;
    public static bool fifty;
    public static bool ninety;
    [SerializeField] Vector3 destination;
    GameObject player;

	// Use this for initialization
	void Start () {

        fifty = false;
        ninety = false;
        player = GameObject.Find("Player");

        teleport.enableEmission = false;
        endGame.enableEmission = false;
	}
	
	// Update is called once per frame
	void Update () {

        if (insanitySlider.value >= 50 && insanitySlider.value <90 && !fifty)
        {
            teleport.enableEmission = true;
            fifty = true;
        }

        if (insanitySlider.value >=90 && !ninety)
        {
            teleport.enableEmission = false;
            endGame.enableEmission = true;
            ninety = true;
        }
		
	}

    private void OnTriggerEnter(Collider other)
    {
        
        if (fifty)
        {
            player.transform.position = destination;
        }

        if (ninety)
        {
            //koniec gry
        }
    }
}
