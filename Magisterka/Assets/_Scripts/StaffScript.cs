﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaffScript : MonoBehaviour {

    [SerializeField] Light left;
    [SerializeField] Light right;
    [SerializeField] GameObject player;

    float dist;

    Color[] colors = new Color[6];

	// Use this for initialization
	void Start () {
        colors[0] = Color.red;
        colors[1] = Color.green;
        colors[2] = Color.blue;
        colors[3] = Color.cyan;
        colors[4] = Color.magenta;
        colors[5] = Color.yellow;

        left.color = colors[Random.Range(0, 6)];
        right.color = colors[Random.Range(0, 6)];

    }

	
	// Update is called once per frame
	void Update () {

        dist = Vector3.Distance(transform.position, player.transform.position);

        if (dist <= 15)
        {
            transform.LookAt(player.transform);
        }
		
	}
}
