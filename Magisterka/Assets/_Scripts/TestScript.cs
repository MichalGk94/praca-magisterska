﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestScript : MonoBehaviour {

    [SerializeField] Sprite[] photos = new Sprite[2];
    public Image screen;
    float timer;
    int photoIndex;
    bool black;
    bool photo;

	// Use this for initialization
	void Start () {
        screen.color = Color.black;
        photoIndex = 0;
        screen.sprite = photos[photoIndex];
        black = true;
        photo = false;
        timer = 2.0f;
	}
	
	// Update is called once per frame
	void Update () {

        timer -= Time.deltaTime;

        if (timer <= 0 && black)
        {
            black = false;
            screen.color = Color.white;
            timer = 5.0f;
            photo = true;

        }

        if (timer <= 0 && photo)
        {
            photo = false;
            photoIndex++;
            if (photoIndex < photos.Length)
            {
                screen.color = Color.black;
                screen.sprite = photos[photoIndex];
                timer = 2.0f;
                black = true;

            }
            else
            {
                //wyjście do menu
                Debug.Log("Finito!");
            }

        }

        		
	}

}
