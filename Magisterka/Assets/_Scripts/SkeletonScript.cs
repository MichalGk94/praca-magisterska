﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkeletonScript : MonoBehaviour {

    private int speed = 2;
    Animator animator;
    [SerializeField] Slider insanitySlider;
    private float insanity;
    float timer;
    bool appeared;
    [SerializeField] AudioClip danse;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
        animator.enabled = false;
        timer = Random.Range(3, 8);
        appeared = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (GlobalScript.cemeteryEntered)
        {
            timer -= Time.deltaTime;
            if(!TowerScript.Instance.sounds.isPlaying)
            {
                TowerScript.Instance.sounds.clip = danse;
                TowerScript.Instance.sounds.Play();
            }

            if (timer <= 0 && !appeared)
            {
                //blackscreen
                BlackScreen.showBlackScreen(BlackScreen.Instance.screen);

                insanity = insanitySlider.value + Random.Range(2, 4);
                insanitySlider.value = insanity;

                transform.position = new Vector3(transform.position.x, 231.2f, transform.position.z);
                animator.enabled = true;
                appeared = true;
            }

        }

        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Walk01"))
        {
            transform.Rotate(0, Time.deltaTime * speed*10, 0);
        }

        if (animator.GetCurrentAnimatorStateInfo(0).IsName("WalkLeft"))
        {
            transform.Translate(Vector3.left * Time.deltaTime * speed, Space.Self);
        }

    }
}