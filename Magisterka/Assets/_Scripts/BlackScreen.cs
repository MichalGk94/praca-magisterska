﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackScreen : MonoBehaviour {

    public static BlackScreen Instance;
    public GameObject screen;

    private void Awake()
    {
        screen = Resources.Load<GameObject>("Screen");
    }

    // Use this for initialization
    void Start () {
        Instance = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public static void showBlackScreen(GameObject gameObject)
    {
        GameObject Screen = Instantiate(gameObject);
        Screen.transform.SetParent(GameObject.Find("Canvas").transform);
        Screen.transform.localPosition = new Vector3(0, 0, 0);
        Object.Destroy(Screen, 1);
    }

}
