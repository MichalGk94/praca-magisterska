﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipScript : MonoBehaviour {

    private Vector3 wreck_pos;
    GameObject player;
    GameObject wreckage;
    private bool shipRange;
    [SerializeField] Slider insanitySlider;
    private float insanity;

    // Use this for initialization
    void Start () {

        wreck_pos = TowerScript.wreck_pos;
        shipRange = false;
        player = GameObject.Find("Player");
        wreckage = Resources.Load<GameObject>("Wreck");
		
	}
	
	// Update is called once per frame
	void Update () {

        if (GlobalScript.ship_appeared)
        {

            if (Vector3.Distance(transform.position, player.transform.position) <= 250)
                shipRange = true;

            Debug.Log(Vector3.Distance(transform.position, wreck_pos) <= 35 && shipRange);

            if (Vector3.Distance(transform.position, wreck_pos) <= 35 && shipRange)
            {

                //blackscreen
                BlackScreen.showBlackScreen(BlackScreen.Instance.screen);

                //dźwięk pioruna i śmiech
                TowerScript.Instance.sounds.clip = TowerScript.Instance.Thunder;
                TowerScript.Instance.sounds.Play();
                TowerScript.Instance.sounds.clip = TowerScript.Instance.Laugh;
                TowerScript.Instance.sounds.Play();
                GameObject.Instantiate(wreckage, transform.position, Quaternion.Euler(-18.0f, -120.0f, 7.5f));
                insanity = insanitySlider.value + Random.Range(20, 26);
                insanitySlider.value = insanity;
                Destroy(gameObject);

            }
        }	
	}
}
