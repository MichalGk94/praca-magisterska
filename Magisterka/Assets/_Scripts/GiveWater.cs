﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiveWater : MonoBehaviour {

    public LayerMask m_PlayerMask;
    public float m_StartingWater = 25f;
    private float m_CurrentWater;
    private float radius = 5f;
    private bool wasVisited;

    // Use this for initialization
    void Start()
    {
        m_CurrentWater = m_StartingWater;
        wasVisited = false;
    }

    private void OnTriggerEnter(Collider other)
    {

        if (!wasVisited)
        {
            
        Collider[] colliders = Physics.OverlapCapsule(new Vector3(846f,118f,882f), new Vector3(833.5f,118f,867.35f), radius, m_PlayerMask);

            for (int i = 0; i < colliders.Length; i++)
            {
                Rigidbody targetRigidbody = colliders[i].GetComponent<Rigidbody>();

                if (!targetRigidbody)
                {
                    continue;
                }

                UnityStandardAssets.Characters.FirstPerson.FirstPersonController targetPerson =
                    targetRigidbody.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>();

                if (!targetPerson)
                {
                    continue;
                }

                //targetPerson.GetWater(m_CurrentWater);

                wasVisited = true;

            }
        }
    }
}
