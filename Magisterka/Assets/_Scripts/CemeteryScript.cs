﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CemeteryScript : MonoBehaviour {

    private float countdown;
    private bool inRange;
    [SerializeField] GameObject Player;
    public static bool moved;

    // Use this for initialization
    void Start () {
        this.GetComponentInChildren<LineRenderer>().enabled = false;
        countdown = 5.0f;
        inRange = false;
	}
	
	// Update is called once per frame
	void Update () {
        if(TowerScript.entered == true)
        {
            this.GetComponentInChildren<LineRenderer>().enabled = true;
        }

        float dist = Vector3.Distance(Player.transform.position, transform.position);
        if (dist <= 100 && !inRange) inRange = true;
        if (dist > 100 && inRange) inRange = false;

        if (inRange)
        {
            if (!GetComponent<AudioSource>().isPlaying) countdown -= Time.deltaTime;

            if (countdown <= 0.0f)
            {
                GetComponent<AudioSource>().Play();
                countdown = 5.0f;
            }

        }

    }
}
