﻿//Script made by StrupsGames

var ani : Animator;
var sound : AudioClip;

function Start () {
	ani.enabled = false;
}

function OnTriggerEnter () {
	AudioSource.PlayClipAtPoint(sound, transform.position);
	ani.enabled = true;
	Destroy(gameObject);
}

