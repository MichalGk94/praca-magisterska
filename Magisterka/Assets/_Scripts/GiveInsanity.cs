﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiveInsanity : MonoBehaviour {

    public LayerMask m_PlayerMask;
    public float radius;
    public float Insanity = 10f;
    private float m_CurrentInsanity;
    private bool wasVisited;

	// Use this for initialization
	void Start () {

        wasVisited = false;
        m_CurrentInsanity = Insanity;

	}

    private void OnTriggerEnter(Collider other)
    {
        if (!wasVisited)
        {

            Collider[] colliders = Physics.OverlapSphere(transform.position, radius, m_PlayerMask);

            for(int i=0; i < colliders.Length; i++)
            {
                Rigidbody targetRigidbody = colliders[i].GetComponent<Rigidbody>();
                if (!targetRigidbody)
                {
                    continue;
                }

                UnityStandardAssets.Characters.FirstPerson.FirstPersonController targetPerson =
                   targetRigidbody.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>();

                if (!targetPerson)
                {
                    continue;
                }

                //targetPerson.GetInsanity(m_CurrentInsanity);

                wasVisited = true;
            }

        }
    }

}
